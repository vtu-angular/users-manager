export class User {
    id!: number;
    username!: string;
    email!: string;
    phoneNumber!: string;
    age!: number;
    creationDate!: Date;
    isActive!: boolean;
}
