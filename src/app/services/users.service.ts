import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import {Observable} from "rxjs";
import {User} from "../models/user.model";

@Injectable({
    providedIn: 'root'
})
export class UsersService {

    constructor(private httpClient: HttpClient) {
    }

    getAll(): Observable<User[]> {
        return this.httpClient.get<User[]>('http://localhost:3000/users');
    }

}
