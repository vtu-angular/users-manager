import { Component, OnInit } from '@angular/core';
import { User } from "../models/user.model";
import { UsersService } from "../services/users.service";

@Component({
  selector: 'um-playground',
  templateUrl: './playground.component.html',
  styleUrls: ['./playground.component.scss']
})
export class PlaygroundComponent implements OnInit {

  name: string;
  counter: number;

  isTableVisible: boolean;

  users: User[];

  constructor(private usersService: UsersService) {
    this.name = 'John';
    this.counter = 0;
    this.isTableVisible = false;

    this.users = [
      {
        id: 1,
        username: 'admin',
        email: 'admin@mail.com',
        phoneNumber: '+359874123567',
        age: 25,
        creationDate: new Date(),
        isActive: true
      },
      {
        id: 2,
        username: 'john',
        email: 'john@mail.com',
        phoneNumber: '+359834123367',
        age: 32,
        creationDate: new Date(),
        isActive: true
      },
      {
        id: 3,
        username: 'jack',
        email: 'jack@mail.com',
        phoneNumber: '+35962367642',
        age: 21,
        creationDate: new Date(),
        isActive: false
      },
      {
        id: 4,
        username: 'jane',
        email: 'jane@mail.com',
        phoneNumber: '+35943657234',
        age: 27,
        creationDate: new Date(),
        isActive: true
      }
    ]
  }

  ngOnInit(): void {
    this.usersService.getAll().subscribe({
      next: (response) => {
        this.users = response;
      }
    })
  }

  toggleTable(): void {
    this.isTableVisible = !this.isTableVisible;
  }

  addUser(): void {
    this.users.push({
      id: 5,
      username: 'steve',
      email: 'steve@mail.com',
      phoneNumber: '+359162537123',
      age: 43,
      creationDate: new Date(),
      isActive: true
    })
  }

  activateUser(user: User): void {
    user.isActive = true;
  }

  deactivateUser(user: User): void {
    user.isActive = false;
  }

  changeName(): void {
    this.name = 'Jack';
  }

  increase(): void {
    this.counter++;
  }

  decrease(): void {
    this.counter--;
  }

}
